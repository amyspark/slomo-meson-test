test: test.c memcpy_aligned-avx.S memcpy_aligned-sse.S video_orc_convert_I420_BGRA-avx.S video_orc_convert_I420_BGRA-sse.S
	$(CC) -o $@ $(shell pkg-config --libs --cflags glib-2.0) $^ -Wall -g -O2

clean:
	rm -f test
