#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <glib.h>

#define N_VARS (64)
#define D1 0
#define D2 1
#define S1 4
#define S2 5
#define S3 6
#define S4 7
#define A2 13
#define P1 24
#define P2 25
#define P3 26
#define P4 27
#define P5 28

typedef struct {
  void *program;
  int n;
  int counter1, counter2, counter3;

  void *arrays[N_VARS];
  int params[N_VARS];
} Executor;

extern void memcpy_aligned_sse(Executor *ex);
extern void memcpy_aligned_avx(Executor *ex);

extern void video_orc_convert_I420_BGRA_sse(Executor *ex);
extern void video_orc_convert_I420_BGRA_avx(Executor *ex);

#define N (100 * 1024 * 1024)
#define LOOP (1000000000)
#define ALIGN (32)

#define W (1920)
#define H (1080)

int main() {
  void *src = aligned_alloc(ALIGN, N + ALIGN);
  void *dest = aligned_alloc(ALIGN, N + ALIGN);

  void *s1 = src;
  s1 += ((uintptr_t) s1 % ALIGN);
  void *s2 = s1 + W * H;
  s2 += ((uintptr_t) s2 % ALIGN);
  void *s3 = s2 + (W * H) / 4;
  s3 += ((uintptr_t) s3 % ALIGN);
  void *d1 = dest + ((uintptr_t) src % ALIGN);

  int p1 = 8;
  int p2 = 100;
  int p3 = 12;
  int p4 = 3;
  int p5 = W;

  GTimer *t = g_timer_new();

  printf("start SSE memcpy\n");
  g_timer_reset(t);
  for (int i = 0; i < LOOP; i++) {
    Executor ex;
    ex.n = N;
    ex.arrays[D1] = d1;
    ex.arrays[S1] = s1;
    ex.arrays[A2] = NULL;

    memcpy_aligned_sse(&ex);
  }
  printf("end SSE memcpy: %lf\n", g_timer_elapsed(t, NULL));

  printf("start AVX memcpy\n");
  g_timer_reset(t);
  for (int i = 0; i < LOOP; i++) {
    Executor ex;
    ex.n = N;
    ex.arrays[D1] = d1;
    ex.arrays[S1] = s1;
    ex.arrays[A2] = NULL;

    memcpy_aligned_avx(&ex);
  }
  printf("end AVX memcpy: %lf\n", g_timer_elapsed(t, NULL));


  printf("start SSE I420->BGRA\n");
  g_timer_reset(t);
  for (int i = 0; i < LOOP; i++) {
    Executor ex;
    ex.n = N;
    ex.arrays[D1] = d1;
    ex.arrays[S1] = s1;
    ex.arrays[S2] = s2;
    ex.arrays[S3] = s3;
    ex.params[P1] = p1;
    ex.params[P2] = p2;
    ex.params[P3] = p3;
    ex.params[P4] = p4;
    ex.params[P5] = p5;
    ex.arrays[A2] = NULL;

    video_orc_convert_I420_BGRA_sse(&ex);
  }
  printf("end SSE I420->BGRA: %lf\n", g_timer_elapsed(t, NULL));

  printf("start AVX I420->BGRA\n");
  g_timer_reset(t);
  for (int i = 0; i < LOOP; i++) {
    Executor ex;
    ex.n = N;
    ex.arrays[D1] = d1;
    ex.arrays[S1] = s1;
    ex.arrays[S2] = s2;
    ex.arrays[S3] = s3;
    ex.params[P1] = p1;
    ex.params[P2] = p2;
    ex.params[P3] = p3;
    ex.params[P4] = p4;
    ex.params[P5] = p5;
    ex.arrays[A2] = NULL;

    video_orc_convert_I420_BGRA_avx(&ex);
  }
  printf("end AVX I420->BGRA: %lf\n", g_timer_elapsed(t, NULL));


  g_timer_destroy(t);

  free(dest);
  free(src);

  return 0;
}
